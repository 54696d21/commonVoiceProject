import datetime
import json
import os

import requests

urlVotes = (
    "https://commonvoice.mozilla.org/api/v1/en/clips/votes/leaderboard?cursor=[1,23565]"
)
urlRecordings = (
    "https://commonvoice.mozilla.org/api/v1/de/clips/leaderboard?cursor=%5B1,23565%5D"
)

r = requests.get(urlVotes)

# print(r.content)
# with ()
utcnow = datetime.datetime.utcnow()
date_time = utcnow.strftime("%Y-%m-%d--%H-%M")
# print(date_time)

FOLDER_NAME = "data"

jsonData = json.loads(r.content)

if not os.path.exists(FOLDER_NAME):
    os.makedirs(FOLDER_NAME)

FILE_NAME = f"{FOLDER_NAME}/data_{date_time}.json"

with open(FILE_NAME, "w") as f:
    f.write(str(jsonData))
