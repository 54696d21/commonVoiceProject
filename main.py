import datetime
import json

import LangList
import requests
from dateutil import parser
from environs import Env
from sqlalchemy import Column, DateTime, Integer, String, create_engine, sql
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.expression import insert
from sqlalchemy.sql.sqltypes import Date
from utils import is_debug


engine = create_engine("sqlite:///database/datastore.db", echo=False)
Base = declarative_base()


class languageContribution(Base):
    __tablename__ = "languageContributions"

    id = Column(Integer, primary_key=True)
    insertTime = Column(DateTime, default=sql.func.now())
    mtimestamp = Column(DateTime)  # timestamp parsed out of the json
    # a modified version of ISO 639-1 is used here, the language codes are obtained from the LangList.py file
    languageCode = Column(String)
    totSpeech = Column(Integer)  # number of total recorded voice seconds
    # number of total validated voice seconds (subset of total speech seconds)
    valSpeech = Column(Integer)


if __name__ == "__main__":

    # database setup
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    session = Session()

    for lang in LangList.langs:
        [[langName, langcode]] = lang.items()
        url = f"https://commonvoice.mozilla.org/api/v1/{langcode}/clips/stats"
        if is_debug():
            print(langName, langcode)
            print(url)

        req = requests.get(url)
        jsonData = json.loads(req.content)
        for datapoint in jsonData:
            date = datapoint.get("date")
            mdate = parser.parse(date)
            totalSpeech = datapoint.get("total")
            validatedSpeech = datapoint.get("valid")

            if (
                langcode == ""
            ):  # name the "all/total language contributions" in the database properly
                languageName = "cumulativeContributions"
            else:
                languageName = langcode

            msg = languageContribution(
                languageCode=languageName,
                mtimestamp=mdate,
                totSpeech=int(totalSpeech),
                valSpeech=int(validatedSpeech),
            )  # build database insert object

            session.add(msg)  # add table row in database

        session.commit()  # execute database commit after each language was retrieved
