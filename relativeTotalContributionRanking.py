import datetime, io
import json, sys
import LangList
import matplotlib.dates as mpl_dates
import matplotlib.pyplot as plt
import requests
import weekdefinition
from dateutil import parser
from environs import Env
from sqlalchemy import Column, DateTime, Integer, String, asc, create_engine, sql
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.expression import insert
from sqlalchemy.sql.sqltypes import Date
import LangListPlot
from utils import is_debug, getLanguageName, plotLang
import telegram
import constants
from imgurpython import ImgurClient


def getLangContrib(langArray):
    langCode = list(langArray.values())[0]
    langName = list(langArray.keys())[0]
    totalSpeechList = list()
    mtimestampList = list()
    queryResult = (databaseSession.query(languageContribution).group_by(languageContribution.mtimestamp).filter_by(languageCode=langCode))
    for row in queryResult:
        if (row.mtimestamp > weekdefinition.BEGIN_WEEK and row.mtimestamp < weekdefinition.END_WEEK):
            # if row.mtimestamp > datetime.datetime(2021, 1, 12, 17, 0, 12):
            # m.append({row.mtimestamp: row.totSpeech})
            totalSpeechList.append(row.totSpeech)
            mtimestampList.append(row.mtimestamp)
    # print(totalSpeechList, langName)
    difference = totalSpeechList[-1] - totalSpeechList[0]

    ranking.append({
        "lang": langCode,
        "timeContribBeginTimeInterval": totalSpeechList[0],
        "timeContrib": difference,
    })


def uploadImgur(im):
    imgur_client = ImgurClient(constants.IMGUR_Client_ID, constants.IMGUR_Client_SECRET)
    im.save("out.jpg")
    link = imgur_client.upload_from_path("out.jpg", config=None, anon=True).get("link")
    return link


def sendTelegramMsg(msg):
    bot = telegram.Bot(token=constants.TELEGRAM_API_KEY)
    bot.sendMessage(chat_id=constants.TELEGRAM_API_CHATID, text=msg)


def sendTelegramPhoto(im):
    im.save("out.jpg")
    bot = telegram.Bot(token=constants.TELEGRAM_API_KEY)
    bot.sendPhoto(chat_id=constants.TELEGRAM_API_CHATID, photo=open("out.jpg", "rb"))


def isFriday():
    if datetime.datetime.today().weekday() == 4:
        return True
    return False


def sendTelegramTrending():
    pass


def sendTelegramTop():
    pass


engine = create_engine("sqlite:///database/datastore.db", echo=False)
Base = declarative_base()


class languageContribution(Base):
    __tablename__ = "languageContributions"

    id = Column(Integer, primary_key=True)
    insertTime = Column(DateTime, default=sql.func.now())
    mtimestamp = Column(DateTime)  # timestamp parsed out of the json
    # a modified version of ISO 639-1 is used here, the language codes are obtained from the LangList.py file
    languageCode = Column(String)
    totSpeech = Column(Integer)  # number of total recorded voice seconds
    # number of total validated voice seconds (subset of total speech seconds)
    valSpeech = Column(Integer)


def getRoundedPercentage(lang):
    rawPercentage = (lang["timeContrib"] / lang["timeContribBeginTimeInterval"]) * 100
    return round(rawPercentage, 3)


if __name__ == "__main__":
    # if (len(sys.argv) > 1):

    # print(sys.argv[1])
    # exit(1)

    # database connection init
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    databaseSession = Session()

    id = Column(Integer, primary_key=True)
    insertTime = Column(DateTime, default=sql.func.now())
    mtimestamp = Column(DateTime)  # timestamp parsed out of the json
    # a modified version of ISO 639-1 is used here, the language codes are obtained from the LangList.py file
    languageCode = Column(String)
    totSpeech = Column(Integer)  # number of total recorded voice seconds
    # number of total validated voice seconds (subset of total speech seconds)
    valSpeech = Column(Integer)

    ranking = list()
    for i in LangListPlot.langs:
        getLangContrib(i)
    sorted_ranked_contributions = sorted(ranking, key=lambda k: k["timeContrib"], reverse=True)
    percentageList = list()

    for lang in sorted_ranked_contributions:
        try:
            if lang["timeContrib"] and lang["timeContribBeginTimeInterval"] >= 0:
                percentage = getRoundedPercentage(lang)
                lang["roundedPercentage"] = percentage
                percentageList.append(lang)
        except Exception as e:
            print(e)

    sortedPercentageList = sorted(percentageList, key=lambda k: k["roundedPercentage"], reverse=True)

    for lang in sortedPercentageList[:3]:
        print(lang)
    print("--------------------------------")

    cleaned_sorted_ranked_contributions = [
        langObj for langObj in sorted_ranked_contributions if langObj.get("lang") != "cumulativeContributions"
    ]  # sort out cumulativeContributions the API returns, that's also part of the list, but should not be in the ranking

    print("sorted ranked contributions")
    for lang in cleaned_sorted_ranked_contributions[:3]:
        print(lang)

    print("--------------------------------")

    topContribLang = cleaned_sorted_ranked_contributions[0].get("lang")
    sTopContribLang = (f"top contributed: {topContribLang}|{getLanguageName(topContribLang)}")

    trendingLang = sortedPercentageList[0].get("lang")
    sTrendingLang = f"trending {trendingLang}: {getLanguageName(trendingLang)}|{sortedPercentageList[0].get('roundedPercentage')}%"

    trendingImg = plotLang(trendingLang, databaseSession, languageContribution)
    topContribLangImg = plotLang(topContribLang, databaseSession, languageContribution)
    
    if isFriday() or (len(sys.argv) > 1):
        if len(sys.argv) > 1:
            if sys.argv[1] != '-f':
                exit(0)

        sendTelegramMsg(sTrendingLang)
        trendingImgur = uploadImgur(trendingImg)
        sendTelegramMsg(trendingImgur)
        # sendTelegramPhoto(trendingImg)

        print("--------------------------------")

        sendTelegramMsg(sTopContribLang)

        topContribLangImgur = uploadImgur(topContribLangImg)
        sendTelegramMsg(topContribLangImgur)
        # sendTelegramPhoto(topContribLangImg)

        # imgur_client = ImgurClient(constants.IMGUR_Client_ID, constants.IMGUR_Client_SECRET)
        # imgur_client.upload_fd(b, config=None, anon=True).get("link")
        # # top_lang_chart_imgur_link = imgur_client.upload_from_path(TOP, config=None, anon=True).get("link")
        # trending_lang_chart_imgur_link = imgur_client.upload_from_path(TRENDING, config=None, anon=True).get("link")

        req = requests.post(
            "https://www.saveriomorelli.com/api/common-voice-android/v2/hall-of-fame/",
            json={
                "top-contributed": f"{topContribLang}",
                "top-trending": f"{trendingLang}",
                "token": f"{constants.HALL_OF_FAME_API_KEY}",
                "link-top-trending": f"{trendingImgur}",
                "link-top-contributed": f"{topContribLangImgur}",
            },
        )
        sendTelegramMsg("updated 'hall of fame' https://www.saveriomorelli.com/commonvoice/halloffame/")
