import LangList
import matplotlib.dates as mpl_dates
import matplotlib.pyplot as plt
import weekdefinition
from dateutil import parser
from environs import Env
from sqlalchemy import Column, DateTime, Integer, String, asc, create_engine, sql
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.expression import insert
from sqlalchemy.sql.sqltypes import Date
from utils import is_debug

engine = create_engine("sqlite:///database/datastore.db", echo=False)
Base = declarative_base()


class languageContribution(Base):
    __tablename__ = "languageContributions"

    id = Column(Integer, primary_key=True)
    insertTime = Column(DateTime, default=sql.func.now())
    mtimestamp = Column(DateTime)  # timestamp parsed out of the json
    # a modified version of ISO 639-1 is used here, the language codes are obtained from the LangList.py file
    languageCode = Column(String)
    totSpeech = Column(Integer)  # number of total recorded voice seconds
    # number of total validated voice seconds (subset of total speech seconds)
    valSpeech = Column(Integer)


if __name__ == "__main__":

# database setup
Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()

    id = Column(Integer, primary_key=True)
    insertTime = Column(DateTime, default=sql.func.now())
    mtimestamp = Column(DateTime)  # timestamp parsed out of the json
    # a modified version of ISO 639-1 is used here, the language codes are obtained from the LangList.py file
    languageCode = Column(String)
    totSpeech = Column(Integer)  # number of total recorded voice seconds
    # number of total validated voice seconds (subset of total speech seconds)
    valSpeech = Column(Integer)

    ranking = list()

    def plotLang(langArray):
        langCode = list(langArray.values())[0]
        langName = list(langArray.keys())[0]
        m = list()
        z = list()
        a = (
            session.query(languageContribution)
            .group_by(languageContribution.mtimestamp)
            .filter_by(languageCode=langCode)
        )

        for row in a:
            if (
                row.mtimestamp > weekdefinition.BEGIN_WEEK
                and row.mtimestamp < weekdefinition.END_WEEK
            ):
                # if row.mtimestamp > datetime.datetime(2021, 1, 12, 17, 0, 12):
                # m.append({row.mtimestamp: row.totSpeech})
                m.append(row.valSpeech)
                z.append(row.mtimestamp)

        difference = m[-1] - m[0]

        ranking.append(
            {
                "lang": langCode,
                "timeContribBeginTimeInterval": m[0],
                "timeContrib": difference,
            }
        )

    import LangListPlot

    for i in LangListPlot.langs:
        plotLang(i)

    my_list = sorted(ranking, key=lambda k: k["timeContrib"])

    w = list(reversed(my_list))

    tempList2 = list()
    for i in w:
        if i["timeContrib"] and i["timeContribBeginTimeInterval"] >= 0:
            percentage = (i["timeContrib"] / i["timeContribBeginTimeInterval"]) * 100
            roundedPercentage = round(percentage, 3)
            i["roundedPercentage"] = roundedPercentage
            tempList2.append(i)

    my_list2 = reversed(sorted(tempList2, key=lambda k: k["roundedPercentage"]))

    for i in my_list2:
        print(i)
