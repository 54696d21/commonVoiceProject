import matplotlib
import string
import ast
import os
import logging
import datetime
import weekdefinition
from environs import Env
import matplotlib.pyplot as plt
from matplotlib.cbook import get_sample_data
from sqlalchemy import Column, DateTime, Integer, String, create_engine, sql
import LangListPlot
from constants import (
    PLOT_WITH_LOGO,
    COLOR,
    FONT,
    FONT_AXIS,
    FONT_SIZE,
    PLOT_TITLE_FONT_SIZE_BIG,
    PLOT_TITLE_FONT_SIZE_SMALL,
    FONTSIZE_AXIS_LABEL,
    SUPER_TITLE_TEXT,
    SUPER_TITLE_FONT_SIZE,
    BOTTOM_TEXT_L,
    BOTTOM_TEXT_R,
    LEFT_TITLE,
)
from matplotlib.ticker import FormatStrFormatter
import io
from PIL import Image

matplotlib.use("Agg")


def getLanguageName(lookupLangCode):
    for idx, lang in enumerate(LangListPlot.langs):
        langCode = LangListPlot.langs[idx].values()
        strLangCode = list(langCode)[0]
        if lookupLangCode == strLangCode:
            return list(LangListPlot.langs[idx].keys())[0]
    raise Exception(f"this language code is unknown: {lookupLangCode}")


def getLogoImg():
    try:
        workingDir = os.getcwd()
        logoImg = plt.imread(get_sample_data(f"{workingDir}/logo.jpg"))
        return logoImg
    except Exception:
        print("could not find logo image file, plotting without logo...")
        PLOT_WITH_LOGO = False


def strfdelta(tdelta, fmt="{D:02}d {H:02}h {M:02}m {S:02}s", inputtype="timedelta"):
    """Convert a datetime.timedelta object or a regular number to a custom-
    formatted string, just like the stftime() method does for datetime.datetime
    objects.

    https://stackoverflow.com/questions/538666/format-timedelta-to-string

    The fmt argument allows custom formatting to be specified.  Fields can
    include seconds, minutes, hours, days, and weeks.  Each field is optional.

    Some examples:
        '{D:02}d {H:02}h {M:02}m {S:02}s' --> '05d 08h 04m 02s' (default)
        '{W}w {D}d {H}:{M:02}:{S:02}'     --> '4w 5d 8:04:02'
        '{D:2}d {H:2}:{M:02}:{S:02}'      --> ' 5d  8:04:02'
        '{H}h {S}s'                       --> '72h 800s'

    The inputtype argument allows tdelta to be a regular number instead of the
    default, which is a datetime.timedelta object.  Valid inputtype strings:
        's', 'seconds',
        'm', 'minutes',
        'h', 'hours',
        'd', 'days',
        'w', 'weeks'
    """

    # Convert tdelta to integer seconds.
    if inputtype == "timedelta":
        remainder = int(tdelta.total_seconds())
    elif inputtype in ["s", "seconds"]:
        remainder = int(tdelta)
    elif inputtype in ["m", "minutes"]:
        remainder = int(tdelta) * 60
    elif inputtype in ["h", "hours"]:
        remainder = int(tdelta) * 3600
    elif inputtype in ["d", "days"]:
        remainder = int(tdelta) * 86400
    elif inputtype in ["w", "weeks"]:
        remainder = int(tdelta) * 604800

    f = string.Formatter()
    desired_fields = [field_tuple[1] for field_tuple in f.parse(fmt)]
    possible_fields = ("W", "D", "H", "M", "S")
    constants = {"W": 604800, "D": 86400, "H": 3600, "M": 60, "S": 1}
    values = {}
    for field in possible_fields:
        if field in desired_fields and field in constants:
            values[field], remainder = divmod(remainder, constants[field])
    return f.format(fmt, **values)


def is_debug():
    env = Env()
    env.read_env()  # read .env file, if it exists
    if ast.literal_eval(env("DEBUG")):
        return True
    else:
        return False


# def defSessionBuilder():
#     # database setup
#     Base.metadata.create_all(engine)
#     Session = sessionmaker(bind=engine)
#     session = Session()

# class languageContribution(Base):
#     __tablename__ = "languageContributions"

#     id = Column(Integer, primary_key=True)
#     insertTime = Column(DateTime, default=sql.func.now())
#     mtimestamp = Column(DateTime)  # timestamp parsed out of the json
#     # a modified version of ISO 639-1 is used here, the language codes are obtained from the LangList.py file
#     languageCode = Column(String)
#     totSpeech = Column(Integer)  # number of total recorded voice seconds
#     # number of total validated voice seconds (subset of total speech seconds)
#     valSpeech = Column(Integer)


# def plotLang(langArray, databaseSession, languageContribution):
def plotLang(langCode, databaseSession, languageContribution):
    # langCode = list(langArray.values())[0]
    # langName = list(langArray.keys())[0]
    langName = getLanguageName(langCode)

    contributionSecondsList = list()
    timestampList = list()
    db_query_out = (
        databaseSession.query(languageContribution)
        .group_by(languageContribution.mtimestamp)
        .filter_by(languageCode=langCode)
    )

    for row in db_query_out:
        if (
            row.mtimestamp > weekdefinition.BEGIN_WEEK
            and row.mtimestamp < weekdefinition.END_WEEK
        ):
            contributionSecondsList.append(row.totSpeech)
            # contributionSecondsList.append(row.valSpeech)
            timestampList.append(row.mtimestamp)

    contributionHoursRoundedList = [
        round((x / 60 / 60), 2) for x in contributionSecondsList
    ]

    plt.clf()  # clear current figure
    fig, ax = plt.subplots()
    plt.rcParams["font.size"] = FONT_SIZE
    plt.rcParams["font.family"] = FONT_AXIS
    plt.rcParams["date.autoformatter.day"] = "%d/%m"

    ax.plot_date(
        timestampList,
        contributionHoursRoundedList,
        linestyle="solid",
        color=COLOR,
        label="",
        marker="",
    )  # dots on line

    fig.autofmt_xdate()
    plt.suptitle(SUPER_TITLE_TEXT, fontsize=SUPER_TITLE_FONT_SIZE, y=1.00)

    weeksContributionsSeconds = contributionSecondsList[-1] - contributionSecondsList[0]
    weeksContributionsTimedelta = datetime.timedelta(seconds=weeksContributionsSeconds)

    if weeksContributionsSeconds > 0:
        if weeksContributionsSeconds <= 0:
            plt.title(
                "no new recordings", loc="left", fontsize=PLOT_TITLE_FONT_SIZE_SMALL
            )
            plt.title(f"{langName}", fontsize=PLOT_TITLE_FONT_SIZE_BIG)

        elif weeksContributionsSeconds >= 300:
            plt.title(f"{langName}", fontsize=PLOT_TITLE_FONT_SIZE_BIG)
            plt.title(
                f"{LEFT_TITLE}{strfdelta(weeksContributionsTimedelta, '{H}h{M:02}min')}",
                loc="left",
                fontsize=PLOT_TITLE_FONT_SIZE_SMALL,
            )
        elif weeksContributionsSeconds < 300:
            plt.title(f"{langName}", fontsize=PLOT_TITLE_FONT_SIZE_BIG)
            plt.title(
                "less than \n5 min recorded",
                loc="left",
                fontsize=PLOT_TITLE_FONT_SIZE_SMALL,
            )

        if langCode == "cumulativeContributions":
            print("cumulativeContributions")
            plt.title("all combined", fontsize=PLOT_TITLE_FONT_SIZE_BIG)
            plt.title(
                f"{weekdefinition.BEGIN_WEEK.year}",
                loc="right",
                fontsize=PLOT_TITLE_FONT_SIZE_SMALL,
            )
        else:
            plt.title(
                f"{weekdefinition.BEGIN_WEEK.year}\nISO-639: {langCode}",
                loc="right",
                fontsize=PLOT_TITLE_FONT_SIZE_SMALL,
            )
            plt.title(f"{langName}", fontsize=PLOT_TITLE_FONT_SIZE_BIG)

        ax.set_xlabel("Days", fontsize=FONTSIZE_AXIS_LABEL)
        ax.set_ylabel("Hours", fontsize=FONTSIZE_AXIS_LABEL)
        ax.margins()
        ax.yaxis.set_major_formatter(FormatStrFormatter("%.1f"))

        ax.text(
            -0.149,  # x axis
            -0.3,  # y axis
            BOTTOM_TEXT_L,
            horizontalalignment="left",
            verticalalignment="top",
            transform=ax.transAxes,
            color="grey",
        )

        ax.text(
            0.5,  # x axis
            -0.3,  # y axis
            BOTTOM_TEXT_R,
            horizontalalignment="left",
            verticalalignment="top",
            transform=ax.transAxes,
            color="grey",
        )

        now = datetime.datetime.utcnow()
        date_time = now.strftime("%Y-%m-%d")

        # create the current weeks folder if it not exists
        os.makedirs(weekdefinition.FOLDER_NAME + "_continousTime", exist_ok=True)

        # Change the numbers in this array to position your image [left, bottom, width, height])
        ax = plt.axes([0.00, 0.9, 0.10, 0.10], frameon=True)
        if PLOT_WITH_LOGO:
            ax.imshow(getLogoImg())
        ax.axis("off")  # get rid of the ticks and ticklabels

        print(f"plotting {langName}...")
        plt.savefig(
            f"{weekdefinition.FOLDER_NAME}_continousTime/{langName}-{date_time}.png",
            dpi=400,
            pad_inches=0.2,
            bbox_inches="tight",
        )

        buf = io.BytesIO()
        plt.savefig(buf, format="png", bbox_inches="tight", pad_inches=0.2, dpi=400)
        im = Image.open(buf)
        buf2 = io.BytesIO()
        plt.savefig(buf2, format="jpg", bbox_inches="tight", pad_inches=0.2, dpi=400)
        im = Image.open(buf2)
        # im.show()
        # buf.close()
        plt.close("all")  # clean up memory
        # return im, buf, buf2
        return im

    else:
        print(f"no new recorded audio for {langName}, so plot is not saved")
        buf = io.BytesIO()
