import re

import requests
from bs4 import BeautifulSoup

# create function to pull letter from webpage (pulls text within <p> elements


def pull_letter(http):
    print(http)
    # get html from webpage given by 'http'
    html = requests.get(http).text
    # parse into a beautiful soup object
    soup = BeautifulSoup(html, "html.parser")

    # build text contents within all p elements
    txt = "\n".join([x.text for x in soup.find_all("p")])
    # replace extended whitespace with single space
    txt = txt.replace("  ", " ")
    # replace webpage references ('[1]', '[2]', etc)
    txt = re.sub("\[\d+\]", "", txt)
    # replace all number bullet points that Seneca uses ('1.', '2.', etc)
    txt = re.sub("\d+. ", "", txt)
    # remove double newlines
    txt = txt.replace("\n\n", "\n")
    # and return the result
    return txt


# import page containing links to all of Seneca's letters
# get web address
src = "https://en.wikisource.org/wiki/Moral_letters_to_Lucilius"

html = requests.get(src).text  # pull html as text
soup = BeautifulSoup(html, "html.parser")  # parse into BeautifulSoup object

letters_regex = re.compile("^Letter\s+[0-9]{1,3}$")

for x in soup.find_all("a"):
    if len(x.contents) > 0:
        if letters_regex.match(str(x.contents[0])):
            print(f'{x.contents[0]} = {x.get("href")}')


moral_letters = {
    x.contents[0]: [
        x.get("href"),
        pull_letter(f"https://en.wikisource.org{x.get('href')}"),
    ]
    for x in soup.find_all("a")
    if len(x.contents) > 0
    if letters_regex.match(str(x.contents[0]))
}  # pull_letter function defined in hello_lucilius_letter.py
