import datetime
import json

import LangList

# plotLang('de')
import LangListPlot
import matplotlib
import matplotlib.dates as mpl_dates
import matplotlib.pyplot as plt
import requests
import weekdefinition
from dateutil import parser
from environs import Env
from sqlalchemy import Column, DateTime, Integer, String, asc, create_engine, sql
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.expression import insert
from sqlalchemy.sql.sqltypes import Date
from utils import is_debug

engine = create_engine("sqlite:///database/datastore.db", echo=False)
Base = declarative_base()


class languageContribution(Base):
    __tablename__ = "languageContributions"

    id = Column(Integer, primary_key=True)
    insertTime = Column(DateTime, default=sql.func.now())
    mtimestamp = Column(DateTime)  # timestamp parsed out of the json
    # a modified version of ISO 639-1 is used here, the language codes are obtained from the LangList.py file
    languageCode = Column(String)
    totSpeech = Column(Integer)  # number of total recorded voice seconds
    # number of total validated voice seconds (subset of total speech seconds)
    valSpeech = Column(Integer)


if __name__ == "__main__":

    # database setup
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    session = Session()

    id = Column(Integer, primary_key=True)
    insertTime = Column(DateTime, default=sql.func.now())
    mtimestamp = Column(DateTime)  # timestamp parsed out of the json
    # a modified version of ISO 639-1 is used here, the language codes are obtained from the LangList.py file
    languageCode = Column(String)
    totSpeech = Column(Integer)  # number of total recorded voice seconds
    # number of total validated voice seconds (subset of total speech seconds)
    valSpeech = Column(Integer)

    def plotLang(langArray):
        langCode = list(langArray.values())[0]
        # langCode = list(langArray.keys())[0]
        langName = list(langArray.keys())[0]
        # print("------")
        # print(langCode)
        # print("------")
        # language = "lt"
        # language = "cumulativeContributions"
        m = list()
        z = list()
        a = (
            session.query(languageContribution)
            .group_by(languageContribution.mtimestamp)
            .filter_by(languageCode=langCode)
        )
        # a = session.query(languageContribution).distinct(
        #     languageContribution.mtimestamp).count()
        # a = session.query(languageContribution).distinct(languageContribution.mtimestamp).filter_by(
        # languageCode=language)
        # select([func.count(distinct(users_table.c.name))])

        for row in a:
            # for row in session.query(languageContribution).filter_by(languageCode=language).distinct(languageContribution.totSpeech).order_by(asc(languageContribution.mtimestamp)):
            # for user in session.query(languageContribution).filter_by(languageCode='de').filter_by(mtimestamp > datetime.datetime(2021, 01, 12, 17, 0, 12)):
            # if row.mtimestamp > datetime.datetime(2021, 1, 12, 17, 0, 12):
            if (
                row.mtimestamp > weekdefinition.BEGIN_WEEK
                and row.mtimestamp < weekdefinition.END_WEEK
            ):
                # m.append({row.mtimestamp: row.totSpeech})
                m.append(row.valSpeech)
                # m.append(row.valSpeech)
                z.append(row.mtimestamp)
                # print(row.mtimestamp)

        k = [round((x / 60 / 60), 2) for x in m]
        # print(k)
        # for i, j in zip(k, z):
        #     print(i, j)

        # print(f"time series has {len(m)} elements")
        plt.clf()
        plt.plot_date(z, k, linestyle="solid", label="points")
        # plt.plot_date(z, k, linestyle="solid", marker='')
        plt.gcf().autofmt_xdate()

        plt.title(f"{langName} ({langCode})")
        plt.xlabel("date")
        plt.ylabel("total recorded audio (in hours)")
        # plt.show()
        now = datetime.datetime.utcnow()
        date_time = now.strftime("%Y-%m-%d")
        FOLDER_NAME = "plotsVal"
        plt.savefig(f"{FOLDER_NAME}/{langName}-{date_time}.png", dpi=200)


for i in LangListPlot.langs:
    # print(list(i.keys())[0])
    plotLang(i)
    # print(list(i.items()))
    # print(list(i[0]))
    # print(list(i.values())[0])
