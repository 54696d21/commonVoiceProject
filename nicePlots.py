import datetime
import os

import LangListPlot
import matplotlib
import matplotlib.pyplot as plt
import weekdefinition
from matplotlib.ticker import FormatStrFormatter
from sqlalchemy import Column, DateTime, Integer, String, create_engine, sql
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from utils import strfdelta, is_debug, plotLang
import operator

matplotlib.use("Agg")

PLOT_WITH_LOGO = True
COLOR = "green"
COLOR2 = "red"
COLOR3 = "blue"
FONT = "Zilla Slab"
# FONT = "Helvetica"
FONT_AXIS = "Lato"
FONT_SIZE = 12
PLOT_TITLE_FONT_SIZE_BIG = 24
PLOT_TITLE_FONT_SIZE_SMALL = 9
FONTSIZE_AXIS_LABEL = 16
SUPER_TITLE_TEXT = "hours contributed in"
SUPER_TITLE_FONT_SIZE = 14
BOTTOM_TEXT_L = "Donate your voice: CV Project"
BOTTOM_TEXT_R = "saveriomorelli.com/commonvoice"
# LEFT_TITLE = "this week\nrecorded:\n"
# LEFT_TITLE = "this month\nrecorded:\n"
LEFT_TITLE = "this time interval\nrecorded:\n"

engine = create_engine("sqlite:///database/datastore.db", echo=False)
Base = declarative_base()


class languageContribution(Base):
    __tablename__ = "languageContributions"

    id = Column(Integer, primary_key=True)
    insertTime = Column(DateTime, default=sql.func.now())
    mtimestamp = Column(DateTime)  # timestamp parsed out of the json
    # a modified version of ISO 639-1 is used here, the language codes are obtained from the LangList.py file
    languageCode = Column(String)
    totSpeech = Column(Integer)  # number of total recorded voice seconds
    # number of total validated voice seconds (subset of total speech seconds)
    valSpeech = Column(Integer)

def customPlot(plotObject, timeList, eventList, lineColor):
    plotObject(
        timeList,
        eventList,
        linestyle="solid",
        color=lineColor,
        label="",
        marker="",
    )  # dots on line


def plotLang(langArray):
    langCode = list(langArray.values())[0]
    langName = list(langArray.keys())[0]

    contributionSecondsList = list()
    validatedSecondsList = list()
    timestampList = list()
    db_query_out = (databaseSession.query(languageContribution).group_by(languageContribution.mtimestamp).filter_by(languageCode=langCode))

    for row in db_query_out:
        if (row.mtimestamp > weekdefinition.BEGIN_WEEK and row.mtimestamp < weekdefinition.END_WEEK):
            contributionSecondsList.append(row.totSpeech)
            validatedSecondsList.append(row.valSpeech)
            timestampList.append(row.mtimestamp)

    contributionHoursRoundedList = [round((x / 60 / 60), 2) for x in contributionSecondsList]
    validatedSecondsRoundedList = [round((x / 60 / 60), 2) for x in validatedSecondsList]

    delta = list(map(operator.sub, contributionHoursRoundedList, validatedSecondsRoundedList))

    plt.clf()  # clear current figure
    fig, ax = plt.subplots()
    plt.rcParams["font.size"] = FONT_SIZE
    plt.rcParams["font.family"] = FONT_AXIS
    plt.rcParams["date.autoformatter.day"] = "%d/%m"

    customPlot(ax.plot_date, timestampList, contributionHoursRoundedList, COLOR)
    customPlot(ax.plot_date, timestampList, validatedSecondsRoundedList, COLOR2)
    # customPlot(ax.plot_date, timestampList, delta, COLOR3)
    # ax.plot_date(
    #     timestampList,
    #     contributionHoursRoundedList,
    #     linestyle="solid",
    #     color=COLOR,
    #     label="",
    #     marker="",
    # )  # dots on line

    # ax.plot_date(
    #     timestampList,
    #     validatedSecondsRoundedList,
    #     linestyle="solid",
    #     color=COLOR2,
    #     label="",
    #     marker="",
    # )  # dots on line

    # ax.plot_date(
    #     timestampList,
    #     delta,
    #     linestyle="solid",
    #     color=COLOR3,
    #     label="",
    #     marker="",
    # )  # dots on line

    fig.autofmt_xdate()
    plt.suptitle(SUPER_TITLE_TEXT, fontsize=SUPER_TITLE_FONT_SIZE, y=1.00)

    weeksContributionsSeconds = contributionSecondsList[-1] - contributionSecondsList[0]
    weeksContributionsTimedelta = datetime.timedelta(seconds=weeksContributionsSeconds)

    if weeksContributionsSeconds > 0:
        if weeksContributionsSeconds <= 0:
            plt.title("no new recordings", loc="left", fontsize=PLOT_TITLE_FONT_SIZE_SMALL)
            plt.title(f"{langName}", fontsize=PLOT_TITLE_FONT_SIZE_BIG)

        elif weeksContributionsSeconds >= 300:
            plt.title(f"{langName}", fontsize=PLOT_TITLE_FONT_SIZE_BIG)
            plt.title(
                f"{LEFT_TITLE}{strfdelta(weeksContributionsTimedelta, '{H}h{M:02}min')}",
                loc="left",
                fontsize=PLOT_TITLE_FONT_SIZE_SMALL,
            )
        elif weeksContributionsSeconds < 300:
            plt.title(f"{langName}", fontsize=PLOT_TITLE_FONT_SIZE_BIG)
            plt.title(
                "less than \n5 min recorded",
                loc="left",
                fontsize=PLOT_TITLE_FONT_SIZE_SMALL,
            )

        if langCode == "cumulativeContributions":
            print("cumulativeContributions")
            plt.title("all combined", fontsize=PLOT_TITLE_FONT_SIZE_BIG)
            plt.title(
                f"{weekdefinition.BEGIN_WEEK.year}",
                loc="right",
                fontsize=PLOT_TITLE_FONT_SIZE_SMALL,
            )
        else:
            plt.title(
                f"{weekdefinition.BEGIN_WEEK.year}\nISO-639: {langCode}",
                loc="right",
                fontsize=PLOT_TITLE_FONT_SIZE_SMALL,
            )
            plt.title(f"{langName}", fontsize=PLOT_TITLE_FONT_SIZE_BIG)

        ax.set_xlabel("Days", fontsize=FONTSIZE_AXIS_LABEL)
        ax.set_ylabel("Hours", fontsize=FONTSIZE_AXIS_LABEL)
        ax.margins()
        ax.yaxis.set_major_formatter(FormatStrFormatter("%.1f"))

        ax.text(
            -0.149,  # x axis
            -0.3,  # y axis
            BOTTOM_TEXT_L,
            horizontalalignment="left",
            verticalalignment="top",
            transform=ax.transAxes,
            color="grey",
        )

        ax.text(
            0.5,  # x axis
            -0.3,  # y axis
            BOTTOM_TEXT_R,
            horizontalalignment="left",
            verticalalignment="top",
            transform=ax.transAxes,
            color="grey",
        )

        now = datetime.datetime.utcnow()
        date_time = now.strftime("%Y-%m-%d")

        # create the current weeks folder if it not exists
        os.makedirs(weekdefinition.FOLDER_NAME + "_continousTime", exist_ok=True)

        # Change the numbers in this array to position your image [left, bottom, width, height])
        ax = plt.axes([0.00, 0.9, 0.10, 0.10], frameon=True)
        # if PLOT_WITH_LOGO:
        #     ax.imshow(im)
        ax.axis("off")  # get rid of the ticks and ticklabels

        print(f"plotting {langName}...")
        plt.savefig(
            f"{weekdefinition.FOLDER_NAME}_continousTime/{langName}-{date_time}.png",
            dpi=400,
            pad_inches=0.2,
            bbox_inches="tight",
        )
        plt.close("all")  # clean up memory
    else:
        print(f"no new recorded audio for {langName}, so plot is not saved")


if __name__ == "__main__":
    # database setup
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    databaseSession = Session()

    id = Column(Integer, primary_key=True)
    insertTime = Column(DateTime, default=sql.func.now())
    mtimestamp = Column(DateTime)  # timestamp parsed out of the json
    # a modified version of ISO 639-1 is used here, the language codes are obtained from the LangList.py file
    languageCode = Column(String)
    totSpeech = Column(Integer)  # number of total recorded voice seconds
    # number of total validated voice seconds (subset of total speech seconds)
    valSpeech = Column(Integer)

    # plotLang('de')
    plotLang({"Esperanto": "eo"})
    # plotLang({"Italian": "it"})
    # plotLang({"German": "de"})
    langCodeList = [list(x.values())[0] for x in LangListPlot.langs]

    # for lang in langCodeList:
    #     plotImg = plotLang(lang, databaseSession, languageContribution)
    #     if plotImg is not None:
    #         plotImg.show()

    # for lang in list(LangListPlot.langs.values()):
    #     print(lang)
    #     plotImg = plotLang(lang, databaseSession, languageContribution)
    #     if plotImg is not None:
    #         plotImg.show()

    for lang in LangListPlot.langs:
        print(lang)
        # plotImg = plotLang(lang, databaseSession, languageContribution)
        # plotLang({"Esperanto": "eo"})
        # if plotImg is not None:
        #     plotImg.show()

        try:
            plotLang(lang)
            # plotLang({"Esperanto": "eo"})
        except Exception:
            print(f"language could not be plotted! {lang}")
