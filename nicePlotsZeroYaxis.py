import datetime
import os
from string import Formatter

import LangList
import LangListPlot
import matplotlib.dates as mpl_dates
import matplotlib.pyplot as plt
import weekdefinition
from dateutil import parser
from environs import Env
from matplotlib.cbook import get_sample_data
from matplotlib.ticker import FormatStrFormatter
from sqlalchemy import Column, DateTime, Integer, String, asc, create_engine, sql
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.expression import insert
from sqlalchemy.sql.sqltypes import Date
from utils import is_debug, strfdelta, getLogoImg

FONT_AXIS = "Lato"


engine = create_engine("sqlite:///database/datastore.db", echo=False)
Base = declarative_base()


class languageContribution(Base):
    __tablename__ = "languageContributions"

    id = Column(Integer, primary_key=True)
    insertTime = Column(DateTime, default=sql.func.now())
    mtimestamp = Column(DateTime)  # timestamp parsed out of the json
    # a modified version of ISO 639-1 is used here, the language codes are obtained from the LangList.py file
    languageCode = Column(String)
    totSpeech = Column(Integer)  # number of total recorded voice seconds
    # number of total validated voice seconds (subset of total speech seconds)
    valSpeech = Column(Integer)


if __name__ == "__main__":

    # database setup
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    session = Session()

    id = Column(Integer, primary_key=True)
    insertTime = Column(DateTime, default=sql.func.now())
    mtimestamp = Column(DateTime)  # timestamp parsed out of the json
    # a modified version of ISO 639-1 is used here, the language codes are obtained from the LangList.py file
    languageCode = Column(String)
    totSpeech = Column(Integer)  # number of total recorded voice seconds
    # number of total validated voice seconds (subset of total speech seconds)
    valSpeech = Column(Integer)

    def plotLang(langArray):
        langCode = list(langArray.values())[0]
        langName = list(langArray.keys())[0]

        contributionSecondsList = list()
        timestampList = list()
        a = (
            session.query(languageContribution)
            .group_by(languageContribution.mtimestamp)
            .filter_by(languageCode=langCode)
        )

        for row in a:
            if (
                row.mtimestamp > weekdefinition.BEGIN_WEEK
                and row.mtimestamp < weekdefinition.END_WEEK
            ):
                contributionSecondsList.append(row.totSpeech)
                timestampList.append(row.mtimestamp)

        zerodContributionHoursRoundedList = [
            x - contributionSecondsList[0] for x in contributionSecondsList
        ]  # set the first value to zero and all following values accordingly

        contributionHoursRoundedList = [
            round((x / 60 / 60), 2) for x in zerodContributionHoursRoundedList
        ]

        plt.clf()  # clear current figure
        fig, ax = plt.subplots()

        COLOR = "green"
        FONT = "Helvetica"

        plt.rcParams["font.size"] = 12
        plt.rcParams["font.family"] = FONT_AXIS
        plt.rcParams["date.autoformatter.day"] = "%d/%m"

        ax.plot_date(
            timestampList,
            contributionHoursRoundedList,
            linestyle="solid",
            color=COLOR,
            label="",
        )  # dots on line

        fig.autofmt_xdate()

        plt.suptitle("hours contributed in", fontsize=14, y=1.00)

        weeksContributionsSeconds = (
            contributionSecondsList[-1] - contributionSecondsList[0]
        )
        weeksContributionsTimedelta = datetime.timedelta(
            seconds=weeksContributionsSeconds
        )

        if langCode != "cumulativeContributions":
            plt.title(f"ISO-639: {langCode}", loc="right", fontsize=9)
        else:
            print("111111111")
            plt.title("all languages", fontsize=24)

        if weeksContributionsSeconds <= 0:
            plt.title("no new recordings", loc="left", fontsize=9)
            plt.title(f"{langName}", fontsize=24)

        elif weeksContributionsSeconds >= 300:
            plt.title(f"{langName}", fontsize=24)
            plt.title(
                f"recorded:\n{strfdelta(weeksContributionsTimedelta, '{H}h{M:02}min')}",
                loc="left",
                fontsize=9,
            )
        elif weeksContributionsSeconds < 300:
            plt.title(f"{langName}", fontsize=24)
            plt.title("less than 5 min recorded", loc="left", fontsize=9)

        FONTSIZE_AXIS_LABEL = 16
        ax.set_xlabel("Days", fontsize=FONTSIZE_AXIS_LABEL)
        ax.set_ylabel("Hours", fontsize=FONTSIZE_AXIS_LABEL)
        ax.margins()

        ax.yaxis.set_major_formatter(FormatStrFormatter("%.1f"))

        ax.text(
            -0.149,  # x axis
            -0.3,  # y axis
            "Donate your voice: CV Project",
            horizontalalignment="left",
            verticalalignment="top",
            transform=ax.transAxes,
            color="grey",
        )

        ax.text(
            0.5,  # x axis
            -0.3,  # y axis
            "saveriomorelli.com/commonvoice",
            horizontalalignment="left",
            verticalalignment="top",
            transform=ax.transAxes,
            color="grey",
        )

        now = datetime.datetime.utcnow()
        date_time = now.strftime("%Y-%m-%d")

        # create the current weeks folder if it not exists
        os.makedirs(weekdefinition.FOLDER_NAME + "_zeroedTime", exist_ok=True)

        # Change the numbers in this array to position your image [left, bottom, width, height])
        ax = plt.axes([0.00, 0.9, 0.10, 0.10], frameon=True)
        ax.imshow(getLogoImg())
        ax.axis("off")  # get rid of the ticks and ticklabels

        if weeksContributionsSeconds > 0:
            print(f"plotting {langName}...")
            plt.savefig(
                f"{weekdefinition.FOLDER_NAME}_zeroedTime/{langName}-{date_time}.png",
                dpi=400,
                pad_inches=0.2,
                bbox_inches="tight",
            )
        else:
            print(f"no new recorded audio for {langName}, so plot is not saved")


if __name__ == "__main__":
    # plotLang('de')
    for i in LangListPlot.langs:
        plotLang(i)
