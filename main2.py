import datetime
import json

import LangList
import requests
from dateutil import parser
from environs import Env
from sqlalchemy import Column, DateTime, Integer, String, create_engine, sql
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.expression import insert
from sqlalchemy.sql.sqltypes import Date
from utils import is_debug

engine = create_engine("sqlite:///database/datastore_language_stats.db", echo=False)
Base = declarative_base()


class language_stats(Base):
    __tablename__ = "languageData"

    id = Column(Integer, primary_key=True)
    insertTime = Column(DateTime, default=sql.func.now())
    data = Column(String)


if __name__ == "__main__":

    # database setup
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    session = Session()

    reqData = requests.get(
        "https://commonvoice.mozilla.org/api/v1/language_stats"
    ).content

    if is_debug():
        print(reqData)

    msg = language_stats(data=reqData)

    session.add(msg)  # add table row in database
    session.commit()  # execute database commit
