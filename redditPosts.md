Das Common Voice Projekt baut eine freie Sprachdatenbank für Machine Learning auf, um unabhängige Sprachtechnologie zu ermöglichen. Bis zum 20. Juli ist Endspurt für den nächsten Release des Datensatzes im August.


Das Projekt Common Voice von Mozilla baut eine freie Datenbank mit Sprache auf um Open Source Projekten, Startups und mittelständischen Firmen dabei zu helfen Sprachtechnologie zu entwickeln die im Moment nur für die großen IT Firmen finanzierbar ist. In den nächsten Tagen (bis 20. Juli) gibt es einen Endspurt um noch so viel wie möglich in den kommenden Release unterzubringen.


The Common Voice project is building a free language database for machine learning to enable independent language technology. The final spurt for the next release of the data set is until July 20th.