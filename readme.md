# Mozilla Common Voice Archiver & Plotting

the purpose of this project is to archive the number of contributed seconds of speech to the Mozilla Common Voice Project because the API does not provide historical data beyond 1 year in the past.
The code is currently used for publishing plots to https://www.reddit.com/r/cvp - a subreddit for the Common Voice Project.

## install dependencies

`python3 -m pip install -r requirements.txt` or `make install`

## run

this is iterates over all languages defined in LangList.py file and gets the number of recorded seconds and stores them to the database under `database/datastore.db`

it takes around 2 minutes to run, it's intentionally not parallelized to not generate unnecessary server load for the API endpoint

`python3 main.py`

this contacts a second API endpoint that has data for more languages (also languages don't have the voice contribution enabled yet) but lacks historical data

single http get request; terminates quickly

`python3 main2.py`

https://commonvoice.mozilla.org/api/v1/clips/voices

## evaluate the data

all evaluation can be done via make commands

`make getTrendingList` -> prints list
`make getTopList` -> prints list
`make generatePlots` -> writes images to folder and prints progress report

## Disclaimer

significant parts of the code are currently highly unoptimized for speed; the code does much more memory and storage read access than would be strictly necessary and relevant parts have O(n^2) run time complexity (over very large inputs when there is enough data in the database) 
still: with more than a year worth of data it currently takes about a minute on a Cortex A72 to execute the ranking and plotting so this is fine for me for now
