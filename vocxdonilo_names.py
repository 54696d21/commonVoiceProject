#! /usr/local/bin/python
import csv
import re
import subprocess
import sys

try:
    from workers import oldVotes, workerNames
except Exception:
    outfile = open("workers", "w")
    with open("workers.py", "w") as data:
        data.write("oldVotes = {}\nworkerNames = {}")
    outfile.close()
    oldVotes = {}
    workerNames = {}  # User-inputted IDs

csvIn, add, sig, msg = [], [], [], []
newVotes = {}  # Internal dict of add, lig


def parse(entry):
    global add, sig, msg, lig, listLig, oldVotes, newVotes

    if len(entry) == 6:
        add, sig = entry[-3], entry[-2]
        # check if valid address
        validAdd = re.match("M[^ ]*", add)
        if validAdd == None:
            return 0
        msg = str(entry[1])
        listLig = re.findall(
            "http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+",
            msg,
        )
        for i in range(len(listLig)):
            listLig[i] = listLig[i].strip(',`[]"')
            listLig[i] = listLig[i].strip("'")
        lig = set(listLig)
        # check new add
        totalAdd = set()
        for i in oldVotes.keys():
            totalAdd |= {i}
        for i in newVotes.keys():
            totalAdd |= {i}
        totalAdd = list(totalAdd)

        if add not in totalAdd:
            print("New address! Please verify:")
            print(add)
            print(msg)
            while True:
                approve = input("Approve? (y/n) or old worker (o)")
                if approve == "n":
                    return 0
                if approve == "o":
                    # show list of addresses from oldVotes and newVotes
                    c = 0
                    for i in totalAdd:
                        print(c, ": ", workerNames[i], " : ", i, sep="")
                        c += 1
                    match = int(input("Which previous address matches the new one? "))
                    if 0 <= match and match <= c:
                        if totalAdd[match] in oldVotes:
                            oldVotes[add] = oldVotes.pop(totalAdd[match])
                        if totalAdd[match] in newVotes:
                            newVotes[add] = newVotes.pop(totalAdd[match])

                        name = workerNames[totalAdd[match]]
                        workerNames.pop(totalAdd[match])  # Delete old add
                        workerNames.update({add: name})  # Add new add

                        return 1
                if approve == "y":
                    name = input("Enter worker's name: ")
                    workerNames.update({add: name})
                    return 1
        return 1
    return 0


# Open csv file
try:
    csvFile = str(sys.argv[1])
    with open(csvFile) as File:
        reader = csv.reader(
            File, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL
        )
        for row in reader:
            csvIn.append(row)
except Exception:
    print("Usage:\t ", sys.argv[0], "<csv file>")
    sys.exit()

# Parse all add, sig, msg, lig
for i in range(len(csvIn)):
    entry = csvIn[i][1].split(sep="\n")

    if parse(entry) == 1:
        sigVerify = subprocess.run(
            ["myriadcoin-0.16.4/bin/myriadcoin-cli", "verifymessage", add, sig, msg],
            stdout=subprocess.PIPE,
        )
        if "true" in str(sigVerify.stdout):
            # Get video code from video links
            for li in lig:
                lig.remove(li)
                li = re.sub("https://youtu\.be/", "", li)
                li = re.sub("https://www\.youtube\.com/watch\?v=", "", li)
                li = re.sub("https://tubaro\.aperu\.net/v/ytb_", "", li)
                li = re.sub("https://youtube\.com/", "", li)
                li = re.sub("&feature=youtu.be", "", li)
                lig.add(li)
                lig -= {""}  # Remove empties
            listLig = lig

            if add in newVotes:
                newVotes[add] |= lig
            else:
                newVotes.update({add: lig})

# Remove double votes:
for add in oldVotes.keys():
    if add in newVotes.keys():
        newVotes[add] -= oldVotes[add]

# Enforce rules: 1 self-entry, 1 vote; 2 self-entries, 5 votes; 3 self-entries; 9 votes
copy = newVotes.copy()
countSelfLig = set()

for add in newVotes.keys():
    voted = set()
    for add_ in oldVotes.keys():
        # Add all links to voted from oldVotes
        voted |= newVotes[add] & oldVotes[add_]
    for i in newVotes.keys():
        if i != add:
            # Add all links to voted from other add in newVotes
            voted |= newVotes[add] & newVotes[i]
    selfLig = newVotes[add] - voted
    for i in lig - selfLig:
        listLig.remove(i)
    lig = set(listLig)

    #    if len(selfLig) > 3:
    #        selfLig = set(list(selfLig)[0:3])
    if len(voted) == 0:
        del copy[add]
    elif 1 <= len(voted) and len(voted) <= 2 and listLig:
        copy[add] = voted | {listLig[0]}
    elif 3 <= len(voted) and len(voted) <= 5 and listLig:
        copy[add] = voted | {listLig[0:2]}
    else:
        copy[add] = selfLig | voted

    countSelfLig |= selfLig

# Count votes
counts = dict()
for ll in copy.values():
    for l in ll:
        if l in counts:
            counts[l] += 1
        else:
            if l in oldVotes.values():
                counts.update({l: 1})
            else:
                counts.update({l: 0})

# Calculate payouts
numWorkers = int(len(copy))
totalVotes = 0
for c in counts.values():
    totalVotes += c

voteHistory = copy.copy()
for add, lig in oldVotes.items():
    if add in voteHistory.keys():
        voteHistory[add] |= lig
    else:
        voteHistory[add] = lig

outfile = open("workers", "w")
with open("workers.py", "w") as data:
    data.write(
        "oldVotes = " + str(voteHistory) + "\n" + "workerNames = " + str(workerNames)
    )
outfile.close()

# Print output
print("Num workers: ", numWorkers)
print("Total votes: ", totalVotes)
for lig, votes in counts.items():
    if re.match("http[s]://", lig) == False:
        print("https://youtu.be/", end="")
    print(lig, ":", "%.2f" % ((numWorkers * 25) * ((votes) / totalVotes)))

# divide week4.csv manually by weeks
# orrrr.... cheat method: just this once, delete the 3 self-links a week limit. and then put it back on? [seems fair as those quiet weeks skewed payouts anyway]
# calculate all the payouts and pay everyone! publish! catch up

# handle dates / divide votes by weeks ???
# generate ze tx's
