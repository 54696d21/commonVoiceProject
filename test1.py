# import telegram, constants
# import datetime, io
# import json
# import LangList
# import matplotlib.dates as mpl_dates
# import matplotlib.pyplot as plt
# import requests
# import weekdefinition
# from dateutil import parser
# from environs import Env
# from sqlalchemy import Column, DateTime, Integer, String, asc, create_engine, sql
# from sqlalchemy.ext.declarative import declarative_base
# from sqlalchemy.orm import sessionmaker
# from sqlalchemy.sql.expression import insert
# from sqlalchemy.sql.sqltypes import Date
# import LangListPlot
# from utils import is_debug, getLanguageName, plotLang
# import telegram
# import constants
# import os
# from imgurpython import ImgurClient

# engine = create_engine("sqlite:///database/datastore.db", echo=False)
# Base = declarative_base()


# class languageContribution(Base):
#     __tablename__ = "languageContributions"

#     id = Column(Integer, primary_key=True)
#     insertTime = Column(DateTime, default=sql.func.now())
#     mtimestamp = Column(DateTime)  # timestamp parsed out of the json
#     # a modified version of ISO 639-1 is used here, the language codes are obtained from the LangList.py file
#     languageCode = Column(String)
#     totSpeech = Column(Integer)  # number of total recorded voice seconds
#     # number of total validated voice seconds (subset of total speech seconds)
#     valSpeech = Column(Integer)


# Base.metadata.create_all(engine)
# Session = sessionmaker(bind=engine)
# databaseSession = Session()


# def sendTelegramMsg(msg):
#     bot = telegram.Bot(token=constants.TELEGRAM_API_KEY)
#     bot.sendMessage(chat_id=constants.TELEGRAM_API_CHATID, text=msg)


# def sendTelegramPhoto(im):
#     im.save("out.jpg")
#     bot = telegram.Bot(token=constants.TELEGRAM_API_KEY)
#     bot.sendPhoto(chat_id=constants.TELEGRAM_API_CHATID, photo=open("out.jpg", 'rb'))


# a = plotLang('de', databaseSession, languageContribution)
