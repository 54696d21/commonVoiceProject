import calendar
import datetime

import numpy as np


def toTimestamp(d):
    return calendar.timegm(d.timetuple())


arr1 = np.array([toTimestamp(datetime.datetime(2008, 1, d)) for d in range(1, 10)])
arr2 = np.arange(1, 10)

result = np.interp(toTimestamp(datetime.datetime(2008, 1, 5, 12)), arr1, arr2)
print(result)  # Prints 5.5
