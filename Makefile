getTrendingList:
	python3 relativeTotalContributionRanking.py

getTopList:
	python3 totalContributionRanking.py

generatePlots:
	python3 nicePlots.py

italiaCompetition:
	python3 italiaCompetitionLocalized.py

plotNumSpeakers:
	python3 plotSpeakers.py

germanCompetition:
	python3 germanCompetitionLocalized.py

clean:

install:
	python3 -m pip install -r requirements.txt
