import datetime
import json

import LangList
import matplotlib.pyplot as plt
import requests
from dateutil import parser
from environs import Env
from sqlalchemy import Column, DateTime, Integer, String, asc, create_engine, sql
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.expression import insert
from sqlalchemy.sql.sqltypes import Date
from utils import is_debug

engine = create_engine("sqlite:///database/datastore.db", echo=False)
Base = declarative_base()


class languageContribution(Base):
    __tablename__ = "languageContributions"

    id = Column(Integer, primary_key=True)
    insertTime = Column(DateTime, default=sql.func.now())
    mtimestamp = Column(DateTime)  # timestamp parsed out of the json
    # a modified version of ISO 639-1 is used here, the language codes are obtained from the LangList.py file
    languageCode = Column(String)
    totSpeech = Column(Integer)  # number of total recorded voice seconds
    # number of total validated voice seconds (subset of total speech seconds)
    valSpeech = Column(Integer)


if __name__ == "__main__":

    # database setup
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    session = Session()

    id = Column(Integer, primary_key=True)
    insertTime = Column(DateTime, default=sql.func.now())
    mtimestamp = Column(DateTime)  # timestamp parsed out of the json
    # a modified version of ISO 639-1 is used here, the language codes are obtained from the LangList.py file
    languageCode = Column(String)
    totSpeech = Column(Integer)  # number of total recorded voice seconds
    # number of total validated voice seconds (subset of total speech seconds)
    valSpeech = Column(Integer)

    language = "lt"
    m = list()
    for row in (
        session.query(languageContribution)
        .filter_by(languageCode=language)
        .distinct()
        .order_by(asc(languageContribution.totSpeech))
    ):
        # for user in session.query(languageContribution).filter_by(languageCode='de').filter_by(mtimestamp > datetime.datetime(2021, 01, 12, 17, 0, 12)):
        if row.mtimestamp > datetime.datetime(2021, 1, 12, 17, 0, 12):
            # m.append({row.mtimestamp: row.totSpeech})
            m.append(row.totSpeech)
    t = set(m)
    [x for x in t]
    print(f"time series has {len(t)} elements")

    # hoursTimeSeries = [x / 60 / 60 for x in t]
    # hoursTimeSeriesInt = [int(x) for x in hoursTimeSeries]
    # hoursTimeSeriesSorted = sorted(hoursTimeSeriesInt)
    # plt.title(language)
    # plt.xlabel("num data points (around every 12h)")
    # plt.ylabel("total hours audio recorded")

    # plt.plot([c for c in range(len(hoursTimeSeriesSorted))], hoursTimeSeriesSorted)
    # plt.show()
    # plt.savefig("temp.png")

    # test = ["2011", "1998"]
    # [int(year) for year in test if year]
