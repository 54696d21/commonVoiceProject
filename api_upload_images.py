import requests
import json
from imgurpython import ImgurClient
from constants import IMGUR_Client_ID, IMGUR_Client_SECRET, HALL_OF_FAME_API_KEY

TOP = "plotsKW26_continousTime/English-2021-07-03.png"
TRENDING = "plotsKW26_continousTime/Armenian-2021-07-03.png"
# PATH = "plotsKW20_continousTime/Bashkir-2021-05-21.png"

if __name__ == "__main__":
    imgur_client = ImgurClient(IMGUR_Client_ID, IMGUR_Client_SECRET)

    top_lang_chart_imgur_link = imgur_client.upload_from_path(
        TOP, config=None, anon=True
    ).get("link")
    trending_lang_chart_imgur_link = imgur_client.upload_from_path(
        TRENDING, config=None, anon=True
    ).get("link")

    print(top_lang_chart_imgur_link, trending_lang_chart_imgur_link)

    req = requests.post(
        "https://www.saveriomorelli.com/api/common-voice-android/v2/hall-of-fame/",
        json={
            "top-contributed": "en",
            "top-trending": "hy-AM",
            "token": f"{HALL_OF_FAME_API_KEY}",
            "link-top-trending": f"{trending_lang_chart_imgur_link}",
            "link-top-contributed": f"{top_lang_chart_imgur_link}",
        },
    )

    print(req.text)
