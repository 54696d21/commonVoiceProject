import datetime
import json

import LangList
import requests
from dateutil import parser
from environs import Env
from sqlalchemy import Column, DateTime, Integer, String, asc, create_engine, sql
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.expression import insert
from sqlalchemy.sql.sqltypes import Date
from utils import is_debug

engine = create_engine("sqlite:///database/datastore.db", echo=False)
Base = declarative_base()


class languageContribution(Base):
    __tablename__ = "languageContributions"

    id = Column(Integer, primary_key=True)
    insertTime = Column(DateTime, default=sql.func.now())
    mtimestamp = Column(DateTime)  # timestamp parsed out of the json
    # a modified version of ISO 639-1 is used here, the language codes are obtained from the LangList.py file
    languageCode = Column(String)
    totSpeech = Column(Integer)  # number of total recorded voice seconds
    # number of total validated voice seconds (subset of total speech seconds)
    valSpeech = Column(Integer)


if __name__ == "__main__":

    # database setup
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    session = Session()

    id = Column(Integer, primary_key=True)
    insertTime = Column(DateTime, default=sql.func.now())
    mtimestamp = Column(DateTime)  # timestamp parsed out of the json
    # a modified version of ISO 639-1 is used here, the language codes are obtained from the LangList.py file
    languageCode = Column(String)
    totSpeech = Column(Integer)  # number of total recorded voice seconds
    # number of total validated voice seconds (subset of total speech seconds)
    valSpeech = Column(Integer)

    # a = session.query(languageContribution)  # , totSpeech)
    # c = a.all()

    # c = a.distinct(languageContribution.languageCode).all()
    # for i in c:
    #     print(i)
    # for j in i:
    #     print(j)

    # query = languageContribution.select()
    # query = query.where(languageContribution.c.name == 'jack')
    # result = conn.execute(query)
    # for row in result:
    #     print row

    m = list()
    for row in (
        session.query(languageContribution)
        .filter_by(languageCode="de")
        .distinct()
        .order_by(asc(languageContribution.totSpeech))
    ):
        # for user in session.query(languageContribution).filter_by(languageCode='de').filter_by(mtimestamp > datetime.datetime(2021, 01, 12, 17, 0, 12)):
        if row.mtimestamp > datetime.datetime(2021, 1, 12, 17, 0, 12):
            # m.append({row.mtimestamp: row.totSpeech})
            m.append(row.totSpeech)
            # print(row.totSpeech)
            # print(row)
        # {'_sa_instance_state': <sqlalchemy.orm.state.InstanceState object at 0x10ec7d4c0>, 'totSpeech': 3179954, 'mtimestamp': datetime.datetime(2021, 3, 12, 17, 0, 12), 'insertTime': datetime.datetime(2021, 3, 12, 19, 0, 18), 'valSpeech': 2995691, 'languageCode': 'de', 'id': 49895}
    t = set(m)
    print(t)
    [x for x in t]
    for i in t:
        print(i)
    print(f"time series has {len(t)} elements")

    # hoursTimeSeries = [int(x / 60 / 60) for x in t]
    hoursTimeSeries = [x / 60 / 60 for x in t]
    hoursTimeSeriesInt = [int(x) for x in hoursTimeSeries]
    hoursTimeSeriesSorted = sorted(hoursTimeSeriesInt)
    for i in hoursTimeSeriesSorted:
        print(i)

    test = ["2011", "1998"]
    [int(year) for year in test if year]
