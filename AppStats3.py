import datetime
import json

import LangList
import requests
from dateutil import parser
from environs import Env
from sqlalchemy import Binary, Column, DateTime, Integer, String, create_engine, sql
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.expression import insert
from sqlalchemy.sql.sqltypes import Date
from utils import is_debug

engine = create_engine("sqlite:///database/appdata6-singleDay.db", echo=False)
Base = declarative_base()


def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + datetime.timedelta(n)


class Language_stats_app(Base):
    __tablename__ = "languageData"

    id = Column(Integer, primary_key=True)
    insertTime = Column(DateTime, default=sql.func.now())
    data = Column(String)
    startDate = Column(DateTime)


if __name__ == "__main__":

    # database setup
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    session = Session()

    start_date = datetime.date(2020, 4, 25)
    end_date = datetime.date(2021, 3, 13)

    for single_date in daterange(start_date, end_date):
        start = single_date
        # end = single_date + datetime.timedelta(days=1)

        print(f"{start}")
        # print(single_date.strftime("%Y-%m-%d"), "--", (single_date +
        #                                             datetime.timedelta(days=1)).strftime("%Y-%m-%d"))
        # https://www.saveriomorelli.com/api/common-voice-android/v2/app-usage/get/?filter=date&start_date=2020-06-15&end_date=2020-06-15
        url = f"https://www.saveriomorelli.com/api/common-voice-android/v2/app-usage/get/?filter=date&start_date={start.strftime('%Y-%m-%d')}&end_date={start.strftime('%Y-%m-%d')}"
        reqData = requests.get(url).content.decode()
        # msg = Language_stats_app(
        #     data=str(reqData), startDate=datetime.date(start), endDate=datetime.date(end))
        msg = Language_stats_app(data=str(reqData), startDate=start)

        session.add(msg)  # add table row in database
        session.commit()  # execute database commit

    # if is_debug():
    #     print(reqData)

    # msg = Language_stats_app(
    #     data=reqData, startDate=start_date, endDate=end_date)

    # session.add(msg)  # add table row in database
    # session.commit()  # execute database commit

    # url = f"https://www.saveriomorelli.com/api/common-voice-android/v2/app-usage/get/?filter=date&start_date=YYYY-MM-DD&end_date=YYYY-MM-DD"
