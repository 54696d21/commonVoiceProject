import random

import matplotlib.pyplot as plt
import numpy as np
import scipy as sp

# sampledata
x = np.arange(199)
r = np.random.rand(100)
y = np.convolve(r, r)

# plot sampledata
plt.plot(x, y, color="grey")

# smoothen sampledata using a 50 degree polynomial
p = sp.polyfit(x, y, deg=50)
y_ = sp.polyval(p, x)

# plot smoothened data
plt.plot(x, y_, color="r", linewidth=2)

plt.show()


[x for x in random.sample(range(10, 30), 5)]
