import datetime
import os
from string import Formatter

import matplotlib
import matplotlib.pyplot as plt
from environs import Env
from matplotlib.cbook import get_sample_data
from matplotlib.ticker import FormatStrFormatter
from sqlalchemy import Column, DateTime, Integer, String, create_engine, sql
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from utils import is_debug, strfdelta, getLogoImg

matplotlib.use("Agg")

PLOT_WITH_LOGO = True
COLOR = "green"
FONT = "Zilla Slab"
FONT_AXIS = "Lato"
# FONT = "Helvetica"
FONT_SIZE = 12
PLOT_TITLE_FONT_SIZE_BIG = 24
PLOT_TITLE_FONT_SIZE_SMALL = 9
FONTSIZE_AXIS_LABEL = 16
SUPER_TITLE_TEXT = "contributi in"
SUPER_TITLE_FONT_SIZE = 14
BOTTOM_TEXT_L = "Dona la tua voce: CV Project"
BOTTOM_TEXT_R = "saveriomorelli.com/commonvoice"

# BEGIN_WEEK = datetime.datetime(2021, 4, 1, 2, 0, 00)
# BEGIN_WEEK = datetime.datetime(2021, 3, 23, 2, 0, 00)
BEGIN_WEEK = datetime.datetime(2021, 3, 20, 0, 0, 00)
# BEGIN_WEEK = datetime.datetime(2021, 4, 1, 2, 0, 00)
# END_WEEK = BEGIN_WEEK + datetime.timedelta(days=30)
# END_WEEK = BEGIN_WEEK + datetime.timedelta(days=31)
END_WEEK = BEGIN_WEEK + datetime.timedelta(days=50)
FOLDER_NAME = "italiaCompetition"


engine = create_engine("sqlite:///database/datastore.db", echo=False)
Base = declarative_base()


class languageContribution(Base):
    __tablename__ = "languageContributions"

    id = Column(Integer, primary_key=True)
    insertTime = Column(DateTime, default=sql.func.now())
    mtimestamp = Column(DateTime)  # timestamp parsed out of the json
    # a modified version of ISO 639-1 is used here, the language codes are obtained from the LangList.py file
    languageCode = Column(String)
    totSpeech = Column(Integer)  # number of total recorded voice seconds
    # number of total validated voice seconds (subset of total speech seconds)
    valSpeech = Column(Integer)


def plotLang(langArray):
    langCode = list(langArray.values())[0]
    langName = list(langArray.keys())[0]

    contributionSecondsList = list()
    timestampList = list()
    db_query_out = (
        session.query(languageContribution)
        .group_by(languageContribution.mtimestamp)
        .filter_by(languageCode=langCode)
    )

    for row in db_query_out:
        if row.mtimestamp > BEGIN_WEEK and row.mtimestamp < END_WEEK:
            contributionSecondsList.append(row.totSpeech)
            timestampList.append(row.mtimestamp)

    contributionHoursRoundedList = [
        round((x / 60 / 60), 2) for x in contributionSecondsList
    ]

    plt.clf()  # clear current figure
    fig, ax = plt.subplots()
    plt.rcParams["font.size"] = FONT_SIZE
    plt.rcParams["font.family"] = FONT_AXIS
    plt.rcParams["date.autoformatter.day"] = "%d/%m"
    ax.set_ylim(235, 350)
    ax.set_xlim([BEGIN_WEEK, END_WEEK])

    ax.plot_date(
        timestampList,
        contributionHoursRoundedList,
        linestyle="solid",
        color=COLOR,
        label="",
        # marker="."
        marker="",
    )  # dots on line

    fig.autofmt_xdate()
    plt.suptitle(SUPER_TITLE_TEXT, fontsize=SUPER_TITLE_FONT_SIZE, y=1.00)

    weeksContributionsSeconds = contributionSecondsList[-1] - contributionSecondsList[0]
    weeksContributionsTimedelta = datetime.timedelta(seconds=weeksContributionsSeconds)

    if weeksContributionsSeconds > 0:
        if weeksContributionsSeconds > 0:
            plt.title(f"{langName}", fontsize=PLOT_TITLE_FONT_SIZE_BIG)
            plt.title(
                f"questo mese\nsono state registrate:\n{strfdelta(weeksContributionsTimedelta, '{H}h{M:02}min')}",
                loc="left",
                fontsize=PLOT_TITLE_FONT_SIZE_SMALL,
            )
        elif weeksContributionsSeconds < 300:
            plt.title(f"{langName}", fontsize=PLOT_TITLE_FONT_SIZE_BIG)
            plt.title(
                "less than 5 min recorded",
                loc="left",
                fontsize=PLOT_TITLE_FONT_SIZE_SMALL,
            )

        if langCode == "cumulativeContributions":
            print("cumulativeContributions")
            plt.title("all combined", fontsize=PLOT_TITLE_FONT_SIZE_BIG)
        else:
            plt.title(
                f"ISO-639: {langCode}", loc="right", fontsize=PLOT_TITLE_FONT_SIZE_SMALL
            )
            plt.title(f"{langName}", fontsize=PLOT_TITLE_FONT_SIZE_BIG)

        ax.set_xlabel("Giorni", fontsize=FONTSIZE_AXIS_LABEL)
        ax.set_ylabel("Ore", fontsize=FONTSIZE_AXIS_LABEL)
        ax.margins()
        ax.yaxis.set_major_formatter(FormatStrFormatter("%.1f"))

        ax.text(
            -0.149,  # x axis
            -0.3,  # y axis
            BOTTOM_TEXT_L,
            horizontalalignment="left",
            verticalalignment="top",
            transform=ax.transAxes,
            color="grey",
        )

        ax.text(
            0.5,  # x axis
            -0.3,  # y axis
            BOTTOM_TEXT_R,
            horizontalalignment="left",
            verticalalignment="top",
            transform=ax.transAxes,
            color="grey",
        )

        now = datetime.datetime.utcnow()
        date_time = now.strftime("%Y-%m-%d")

        # create the current weeks folder if it not exists
        os.makedirs(FOLDER_NAME + "_continousTime", exist_ok=True)

        # Change the numbers in this array to position your image [left, bottom, width, height])
        ax = plt.axes([0.00, 0.9, 0.10, 0.10], frameon=True)
        if PLOT_WITH_LOGO:
            ax.imshow(im)
        ax.axis("off")  # get rid of the ticks and ticklabels

        print(f"plotting {langName}...")
        plt.savefig(
            f"{FOLDER_NAME}_continousTime/{langName}-{date_time}.png",
            dpi=400,
            pad_inches=0.2,
            bbox_inches="tight",
        )
        plt.close("all")  # clean up memory
    else:
        print(f"no new recorded audio for {langName}, so plot is not saved")


if __name__ == "__main__":
    # database setup
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    session = Session()

    id = Column(Integer, primary_key=True)
    insertTime = Column(DateTime, default=sql.func.now())
    mtimestamp = Column(DateTime)  # timestamp parsed out of the json
    # a modified version of ISO 639-1 is used here, the language codes are obtained from the LangList.py file
    languageCode = Column(String)
    totSpeech = Column(Integer)  # number of total recorded voice seconds
    # number of total validated voice seconds (subset of total speech seconds)
    valSpeech = Column(Integer)

    plotLang({"Italiano": "it"})
