import datetime

# BEGIN_WEEK = datetime.datetime(2020, 3, 1, 21, 0, 00)
# BEGIN_WEEK = datetime.datetime(2020, 3, 5, 21, 0, 00)
# BEGIN_WEEK = datetime.datetime(2020, 9, 2, 21, 0, 00)
# BEGIN_WEEK = datetime.datetime(2020, 6, 2, 21, 0, 00)
# BEGIN_WEEK = datetime.datetime(2021, 3, 2, 21, 0, 00)
# BEGIN_WEEK = datetime.datetime(2021, 3, 5, 21, 0, 00)
# BEGIN_WEEK = datetime.datetime(2021, 3, 12, 21, 0, 00)
# BEGIN_WEEK = datetime.datetime(2021, 3, 19, 21, 0, 00)
# BEGIN_WEEK = datetime.datetime(2021, 3, 26, 21, 0, 00)
# BEGIN_WEEK = datetime.datetime(2021, 4, 9, 21, 0, 00)
# BEGIN_WEEK = datetime.datetime(2021, 4, 23, 21, 0, 00)
# BEGIN_WEEK = datetime.datetime(2021, 4, 30, 21, 0, 00)
# BEGIN_WEEK = datetime.datetime(2021, 5, 7, 21, 0, 00)
# BEGIN_WEEK = datetime.datetime(2021, 5, 14, 21, 0, 00)
# BEGIN_WEEK = datetime.datetime(2021, 5, 21, 21, 0, 00)
# BEGIN_WEEK = datetime.datetime(2021, 5, 28, 21, 0, 00)
# BEGIN_WEEK = datetime.datetime(2021, 6, 11, 21, 0, 00)
# BEGIN_WEEK = datetime.datetime(2021, 6, 18, 21, 0, 00)
# BEGIN_WEEK = datetime.datetime(2021, 6, 25, 21, 0, 00)
# BEGIN_WEEK = datetime.datetime.now() - datetime.timedelta(days=7)
BEGIN_WEEK = datetime.datetime.now() - datetime.timedelta(days=7)
# END_WEEK = BEGIN_WEEK + datetime.timedelta(days=7)
# END_WEEK = BEGIN_WEEK + datetime.timedelta(days=14)
# END_WEEK = BEGIN_WEEK + datetime.timedelta(days=30)
# END_WEEK = BEGIN_WEEK + datetime.timedelta(days=45)
# END_WEEK = BEGIN_WEEK + datetime.timedelta(days=90)
END_WEEK = BEGIN_WEEK + datetime.timedelta(days=7)

weeknumber = datetime.date.today().isocalendar()[1]
# FOLDER_NAME = "plotsKW11b"
FOLDER_NAME = f"plotsKW{weeknumber}f"
