import datetime
import json

import LangList
import requests
from dateutil import parser
from environs import Env
from sqlalchemy import Column, DateTime, Integer, String, create_engine, sql
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.expression import insert
from sqlalchemy.sql.sqltypes import Date
from utils import is_debug

engine = create_engine("sqlite:///database/appdata2.db", echo=False)
Base = declarative_base()


class Language_stats_app(Base):
    __tablename__ = "languageData"

    id = Column(Integer, primary_key=True)
    insertTime = Column(DateTime, default=sql.func.now())
    data = Column(String)
    startDate = Column(DateTime)
    endDate = Column(DateTime)


if __name__ == "__main__":

    # database setup
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    session = Session()

    start_date = "2021-03-07"
    end_date = "2021-03-12"

    # url = f"https://www.saveriomorelli.com/api/common-voice-android/v2/app-usage/get/?filter=date&start_date=YYYY-MM-DD&end_date=YYYY-MM-DD"
    url = f"https://www.saveriomorelli.com/api/common-voice-android/v2/app-usage/get/?filter=date&start_date={start_date}&end_date={end_date}"

    reqData = requests.get(url).content

    if is_debug():
        print(reqData)

    msg = Language_stats_app(data=reqData, startDate=start_date, endDate=end_date)

    session.add(msg)  # add table row in database
    session.commit()  # execute database commit
