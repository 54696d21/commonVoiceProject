import datetime
import json
import os
from string import Formatter

import LangListSpeakers
import matplotlib
import matplotlib.pyplot as plt
import weekdefinition
from environs import Env
from matplotlib.cbook import get_sample_data
from matplotlib.ticker import FormatStrFormatter, MaxNLocator
from numpy.lib.function_base import append
from sqlalchemy import Column, DateTime, Integer, String, create_engine, sql
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from utils import is_debug, getLogoImg

matplotlib.use("Agg")

PLOT_WITH_LOGO = True
COLOR = "green"
FONT = "Zilla Slab"
# FONT = "Helvetica"
FONT_AXIS = "Lato"
FONT_SIZE = 12
PLOT_TITLE_FONT_SIZE_BIG = 24
PLOT_TITLE_FONT_SIZE_SMALL = 9
FONTSIZE_AXIS_LABEL = 16
SUPER_TITLE_TEXT = "hours contributed in"
SUPER_TITLE_FONT_SIZE = 14
BOTTOM_TEXT_L = "Donate your voice: CV Project"
BOTTOM_TEXT_R = "saveriomorelli.com/commonvoice"
LEFT_TITLE = "this week\nrecorded:\n"
# LEFT_TITLE = "this month\nrecorded:\n"

engine = create_engine("sqlite:///database/datastore_language_stats.db", echo=False)
Base = declarative_base()


class language_stats(Base):
    __tablename__ = "languageData"

    id = Column(Integer, primary_key=True)
    insertTime = Column(DateTime, default=sql.func.now())
    data = Column(String)


def plotLang(langArray):
    langCode = list(langArray.values())[0]
    langName = list(langArray.keys())[0]

    db_query_out = session.query(language_stats)
    # PLOT_LANG = "ba"
    # PLOT_LANG = "it"
    # PLOT_LANG = "en"
    # PLOT_LANG = "th"
    # PLOT_LANG = "de"

    speakersL = list()
    timeL = list()
    for row in db_query_out:
        b = json.loads(row.data)
        try:
            for i in b.get("launched"):
                if i.get("locale") == langCode:
                    # print(i, row.insertTime)
                    seconds = i["seconds"]
                    hours = round(seconds / 60 / 60)
                    numSpeakers = i["speakers"]
                    timeL.append(row.insertTime)
                    speakersL.append(numSpeakers)
                    # print(hours, numSpeakers, row.insertTime)
        except Exception as e:
            # print(b)
            pass

    plt.clf()  # clear current figure
    plt.rcParams["font.size"] = 12
    plt.rcParams["font.family"] = FONT
    plt.rcParams["date.autoformatter.day"] = "%d/%m"

    fig, ax = plt.subplots()
    ax.plot_date(
        timeL,
        speakersL,
        linestyle="solid",
        color=COLOR,
        marker="",
        label=" ",
    )  # dots on line
    fig.autofmt_xdate()
    print(f"plotting...{langCode} | number of speakers: {speakersL[-1]}")

    plt.suptitle("number of speakers in", fontsize=14, y=1.00)
    plt.title(f"{langName}", fontsize=24)
    plt.title(f"ISO-639: {langCode}", loc="right", fontsize=9)

    FONTSIZE_AXIS_LABEL = 16
    ax.set_xlabel("Days", fontsize=FONTSIZE_AXIS_LABEL)
    ax.set_ylabel("Speakers", fontsize=FONTSIZE_AXIS_LABEL)
    ax.margins()

    # ax.yaxis.set_major_formatter(FormatStrFormatter("%.1f"))
    # ax.xaxis.get_major_locator().set_params(integer=True)
    # ax.xaxis.get_major_locator().set_params(integer=True)
    ax.yaxis.set_major_locator(MaxNLocator(integer=True))

    ax.text(
        -0.149,  # x axis
        -0.3,  # y axis
        "Donate your voice: CV Project",
        horizontalalignment="left",
        verticalalignment="top",
        transform=ax.transAxes,
        color="grey",
    )

    ax.text(
        0.5,  # x axis
        -0.3,  # y axis
        "saveriomorelli.com/commonvoice",
        horizontalalignment="left",
        verticalalignment="top",
        transform=ax.transAxes,
        color="grey",
    )

    now = datetime.datetime.utcnow()
    date_time = now.strftime("%Y-%m-%d")

    ax = plt.axes([0.00, 0.9, 0.10, 0.10], frameon=True)
    ax.imshow(getLogoImg())
    ax.axis("off")  # get rid of the ticks and ticklabels

    FOLDER = "outTestfolder4"
    if not os.path.exists(FOLDER):
        os.makedirs(FOLDER)

    plt.savefig(
        # f"{weekdefinition.FOLDER_NAME}_zeroedTime/{langName}-{date_time}.png",
        f"{FOLDER}/{date_time}_{langCode}.jpg",
        dpi=400,
        pad_inches=0.2,
        bbox_inches="tight",
    )


if __name__ == "__main__":
    # database setup
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    session = Session()

    for i in LangListSpeakers.langs:
        try:
            plotLang(i)
        except Exception:
            print(f"language could not be plotted! {i}")
